public class WordFrequencies {
    private String wordFrequency;
    private int count;

    public WordFrequencies(String wordFrequency, int count){
        this.wordFrequency = wordFrequency;
        this.count = count;
    }


    public String getWordFrequency() {
        return this.wordFrequency;
    }

    public int getWordCount() {
        return this.count;
    }


}
