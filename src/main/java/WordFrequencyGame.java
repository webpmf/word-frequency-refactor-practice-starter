import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getFormatWordFrequency(String wordStr) {
        try {
                List<WordFrequencies> wordFrequenciesList = getWordFrequenciesList(wordStr);
                Map<String, List<WordFrequencies>> wordFrequencies = groupByWordFrequency(wordFrequenciesList);
                List<WordFrequencies> list = getWordCountList(wordFrequencies);
                sortByWordFrequencyCount(list);
                return getFormatResult(list);
            } catch (Exception e) {
                return CALCULATE_ERROR;
            }
    }

    private void sortByWordFrequencyCount(List<WordFrequencies> wordFrequenciesList) {
        wordFrequenciesList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private String getFormatResult(List<WordFrequencies> wordFrequenciesList) {
        return wordFrequenciesList.stream().map(wordFrequencies ->
                wordFrequencies.getWordFrequency() + " " + wordFrequencies.getWordCount()
        ).collect(Collectors.joining("\n"));
    }

    private List<WordFrequencies> getWordCountList(Map<String, List<WordFrequencies>> groupByWordFrequency) {
        return groupByWordFrequency.entrySet().stream()
                .map(entry -> (new WordFrequencies(entry.getKey(), entry.getValue().size()))).collect(Collectors.toList());
    }

    private List<WordFrequencies> getWordFrequenciesList(String wordStr) {
        String[] inputArr = wordStr.split("\\s+");
        return Arrays.stream(inputArr).map(inputItem -> new WordFrequencies(inputItem, 1)).collect(Collectors.toList());
    }


    private Map<String, List<WordFrequencies>> groupByWordFrequency(List<WordFrequencies> wordFrequenciesList) {
        return wordFrequenciesList.stream().collect(Collectors.groupingBy(WordFrequencies::getWordFrequency));
    }
}
